package com.adapter;

import java.util.ArrayList;

import com.classitem.EventsItem;
import com.example.pocforreminder.R;
import com.example.pocforreminder.R.id;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class EventsAdapter extends ArrayAdapter<EventsItem> {
	
	Context context;
	int layoutResourceId;
	private ArrayList<EventsItem> listItems;

	public EventsAdapter(Context context, int layoutResourceId, 
			ArrayList<EventsItem> listItems) {
		
		super(context, layoutResourceId, listItems);
		// TODO Auto-generated constructor stub
	
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.listItems = listItems;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		final eventsHolder holder;

		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		if (row == null) {
			row = mInflater.inflate(layoutResourceId, parent, false);

			holder = new eventsHolder();
			holder.title = (TextView) row.findViewById(R.id.txtEventName);
			holder.startDate = (TextView) row.findViewById(R.id.txtEventStartDate);
			holder.endDate = (TextView) row.findViewById(R.id.txtEventEndDate);
			
			row.setTag(holder);
		} else {
			holder = (eventsHolder) row.getTag();
		}

		EventsItem events = listItems.get(position);
		holder.title.setText(events.getEventTitle());
		holder.startDate.setText(events.getEventFromDate());
		holder.endDate.setText(events.getEventToDate());
		return row;
	}
	
	static class eventsHolder {
		TextView title;
		TextView startDate;
		TextView endDate;
	}
	
}

package com.example.pocforreminder;

import android.support.v7.app.ActionBarActivity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends ActionBarActivity {

	private EventFragment eventFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button mybutton = (Button) findViewById(R.id.button1);
		mybutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				eventFragment = new EventFragment();
				setFragment(eventFragment);

			}
		});
	}

	public void setFragment(Fragment frag) {
		FragmentManager fm = getFragmentManager();
		if (fm.findFragmentById(R.id.container) == null) {
			fm.beginTransaction().add(R.id.container, frag).commit();

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}

package com.example.pocforreminder;

import com.classitem.EventsItem;
import com.database.DatabaseHandler;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class ReminderActivity extends Activity implements OnClickListener {

	Spinner spinnerReminderTime;
	Spinner spinnerReminderDate;
	EditText edtEventTitle;
	EditText edtEventDescription;
	CheckBox checkPopup;
	CheckBox checkEmail;
	Button btnDone;
	ArrayAdapter<String> notificationAdpter;
	DatabaseHandler databaseHandler;
	EventsItem eventItem;

	private String notificationTime[];
	private String reminderEventId;
	private String reminderEventTitle;
	private String reminderEventDescription;
	private String reminderEventAddress;
	private String reminderEventStartDate;
	private String reminderEventEndDate;
	private String eventStartTime = "10:00";
	private String eventEndTime = "12:00";
	private String userReminderDate = "30/9/1990";
	private String userReminderTime;
	private String userId = "hum001";
	private String reminderNotification;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.reminder_detail_activity);
		getFindViewByid();
		getStringArray();
		getIntentValue();
		getValueFromViews();

		databaseHandler = new DatabaseHandler(getBaseContext());

	}

	public void getFindViewByid() {

		spinnerReminderTime = (Spinner) findViewById(R.id.spinner_reminder_time);
		spinnerReminderDate = (Spinner) findViewById(R.id.spinner_event_date);
		btnDone = (Button) findViewById(R.id.btn_done);
		btnDone.setOnClickListener(this);
		edtEventTitle = (EditText) findViewById(R.id.edt_event_title);
		edtEventDescription = (EditText) findViewById(R.id.edt_event_description);
		edtEventDescription = (EditText) findViewById(R.id.edt_event_description);
		checkPopup = (CheckBox) findViewById(R.id.check_popup);
		checkEmail = (CheckBox) findViewById(R.id.check_email);
		checkPopup.setOnClickListener(this);
		checkEmail.setOnClickListener(this);

	}

	public void getStringArray() {
		notificationTime = getResources().getStringArray(
				R.array.notification_time);

		notificationAdpter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, notificationTime);
		notificationAdpter
				.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
		spinnerReminderTime.setAdapter(notificationAdpter);
	}

	public void getIntentValue() {

		edtEventTitle.setText(getIntent().getExtras().getString("title"));
		edtEventDescription.setText(getIntent().getExtras().getString(
				"description"));

		reminderEventId = getIntent().getExtras().getString("eventid");
		reminderEventStartDate = getIntent().getExtras().getString("startDate");
		reminderEventEndDate = getIntent().getExtras().getString("enddate");
		reminderEventAddress = getIntent().getExtras().getString("address");

		// edtEventTitle.setError("this is called error message");
	}

	public void getValueFromViews() {
		reminderEventTitle = edtEventTitle.getText().toString();
		if (reminderEventTitle.equals("")) {
			edtEventTitle.setError("enter title of the event");
		}
		reminderEventDescription = edtEventDescription.getText().toString();

		if (edtEventDescription.equals("")) {
			edtEventTitle.setError("enter description of the event");
		}

		spinnerReminderTime
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						spinnerReminderTime.setSelection(position);
						userReminderTime = (String) spinnerReminderTime
								.getSelectedItem();

					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_done) {
			eventItem = new EventsItem(reminderEventId, reminderEventTitle,
					reminderEventDescription,
					reminderEventStartDate + ", 2014", reminderEventEndDate
							+ ", 2014", reminderEventAddress, eventStartTime,
					eventEndTime, userReminderTime, userReminderDate, userId,
					reminderNotification);
			Toast.makeText(this, eventItem.toString(), Toast.LENGTH_LONG)
					.show();
			Log.e("EVENT ITEM", eventItem.toString());
			databaseHandler.insert(eventItem);
		} else if (v.getId() == R.id.check_popup) {
			if (checkPopup.isChecked()) {
				reminderNotification = "D";
				checkEmail.setChecked(false);
				Toast.makeText(this, "default checked", Toast.LENGTH_LONG)
						.show();
			} else {

			}
		} else if (v.getId() == R.id.check_email) {
			if (checkEmail.isChecked()) {
				reminderNotification = reminderNotification + "E";
				Toast.makeText(this, "email checked", Toast.LENGTH_LONG).show();
			}
		}

	}
}

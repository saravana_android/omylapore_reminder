package com.example.pocforreminder;

import java.util.ArrayList;

import com.Services.EventsService;
import com.adapter.EventsAdapter;
import com.classitem.EventsItem;
import com.utills.GlobalFeatures;
import com.utills.GlobalVariables;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class EventFragment extends Fragment {
	// private Button btnMore;
	// private ProgressBar pg;
	private static int PAGE = 1;

	private ListView eventsListView;
	private EventsService eventsService;
	private EventsAdapter eventsAdapterObj;
	private static ArrayList<EventsItem> tempEvents;
	private static boolean EVENTS_INITIAL_SERVICE_CALL = true;

	public EventFragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.events_layout, container,
				false);

		eventsListView = (ListView) rootView.findViewById(R.id.events_listview);
		/*
		 * LayoutInflater listInflater = getLayoutInflater(savedInstanceState);
		 * ViewGroup footer = (ViewGroup) listInflater.inflate(
		 * R.layout.listview_footer, eventsListView, false);
		 */
		/*
		 * eventsListView.addFooterView(footer, null, false); btnMore = (Button)
		 * footer.findViewById(R.id.btn_loadMore); pg = (ProgressBar)
		 * footer.findViewById(R.id.progressBar_loadMore);
		 */

		/*
		 * View footerView = ((LayoutInflater) getActivity()
		 * .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
		 * R.layout.listview_footer, eventsListView, false);
		 */

		tempEvents = new ArrayList<EventsItem>();
		eventsService = new EventsService(getActivity());
		eventsAdapterObj = new EventsAdapter(getActivity(),
				R.layout.events_list_row, tempEvents);
		eventsListView.setAdapter(eventsAdapterObj);

		if (GlobalFeatures.haveNetworkConnection(getActivity())) {
			new GetEventsList().execute();
		} else {
			GlobalFeatures.showAlertDialog(getActivity());
		}

		/*
		 * btnMore.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View arg0) { // TODO Auto-generated
		 * method stub EVENTS_INITIAL_SERVICE_CALL = true; PAGE++; if
		 * (GlobalFeatures.haveNetworkConnection(getActivity())) { new
		 * GetEventsList().execute();
		 * 
		 * } else { GlobalFeatures.showAlertDialog(getActivity()); } } });
		 */
		eventsListView
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@SuppressLint("NewApi")
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub

						callEventsDialog(position);
					}
				});

		return rootView;
	}

	public void callEventsDialog(int position) {

		final EventsItem event = GlobalVariables.totalEventsList.get(position);

		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.event_dialog);

		final TextView eventTitle = (TextView) dialog.findViewById(R.id.title);
		TextView eventDate = (TextView) dialog.findViewById(R.id.eventdate);
		TextView eventVenue = (TextView) dialog.findViewById(R.id.eventvenue);
		TextView contact = (TextView) dialog.findViewById(R.id.contactdetails);
		TextView about = (TextView) dialog.findViewById(R.id.about);

		eventTitle.setText(event.getEventTitle());

		Log.e("EVENT_ID", "POSITION " + position);
		Log.e("EVENT_ID", "EVENT_ID" + event.getEventId());

		eventDate.setText(event.getEventFromDate() + " - "
				+ event.getEventToDate());
		eventVenue.setText(event.getEventAddress());
		contact.setText(event.getEventContacPerson() + "\n"
				+ event.getEventContacPhone() + "\n"
				+ event.getEventContactMail());
		about.setText(event.getEventDescription());
		dialog.show();

		TextView dismiss = (TextView) dialog.findViewById(R.id.dismiss);
		dismiss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}

		});
		Button reminder = (Button) dialog.findViewById(R.id.btn_reminder);
		reminder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(),
						ReminderActivity.class);
				intent.putExtra("eventid", event.getEventId());
				Log.e("EVENT_ID", "EVENT_ID" + event.getEventId());
				intent.putExtra("title", event.getEventTitle());
				Log.e("EVENT_TITLE", "EVENT_TITLE" + event.getEventTitle());
				intent.putExtra("description", event.getEventDescription());
				intent.putExtra("startDate", event.getEventFromDate());
				intent.putExtra("enddate", event.getEventToDate());
				intent.putExtra("address", event.getEventAddress());
				startActivity(intent);
			}
		});

	}

	class GetEventsList extends AsyncTask<Void, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// btnMore.setVisibility(View.INVISIBLE);
			// pg.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(Void... params) {
			if (EVENTS_INITIAL_SERVICE_CALL) {
				eventsService.getEvents(String.valueOf(PAGE));
				EVENTS_INITIAL_SERVICE_CALL = false;
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(null);
			tempEvents.clear();
			tempEvents.addAll(GlobalVariables.totalEventsList);
			eventsAdapterObj.notifyDataSetChanged();

			// pg.setVisibility(View.INVISIBLE);
			// btnMore.setVisibility(View.VISIBLE);
			if (Integer.parseInt(EventsItem.getTotalcount()) == GlobalVariables.totalEventsList
					.size()) {
				// pg.setVisibility(View.INVISIBLE);
				// btnMore.setVisibility(View.INVISIBLE);

			}
		}
	}
}

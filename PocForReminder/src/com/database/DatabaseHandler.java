package com.database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.classitem.EventsItem;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DatabaseHandler {
	Context myContext;
	DatabaseHelper databaseHelper;
	SQLiteDatabase database;
	private  String dateConversion=null;
	 
	private static final String REMINDER_TABLE_NAME = "reminder";

	public DatabaseHandler(Context context) {
		myContext = context;
		databaseHelper = new DatabaseHelper(context);
		database = databaseHelper.getWritableDatabase();

	}

	public void insert(EventsItem eventsItem) {

		ContentValues contentValues = new ContentValues();
		
		contentValues.put("reminder_event_id", eventsItem.getEventId());
		contentValues.put("reminder_event_title", eventsItem.getEventTitle());
		contentValues.put("reminder_event_description",
				eventsItem.getEventDescription());
		contentValues.put("event_address", eventsItem.getEventAddress());
		dateConversion=eventsItem.getEventFromDate();		
		contentValues.put("event_statrt_date", dateConversion(dateConversion));
		dateConversion=eventsItem.getEventToDate();
		contentValues.put("event_end_date", dateConversion(dateConversion));
		contentValues.put("event_start_time", eventsItem.getEventStartTime());
		contentValues.put("event_end_time", eventsItem.getEventEndTime());
		contentValues.put("user_reminder_date",
				eventsItem.getUserReminderDate());
		contentValues.put("user_reminder_time",
				eventsItem.getUserReminderTime());

		contentValues.put("user_id", eventsItem.getUserId());
		contentValues.put("notify_reminder_status", eventsItem.getEventId());
		Log.e("CONTENTVALUES", ""+contentValues);
		database.insert(REMINDER_TABLE_NAME, null, contentValues);

	}

	public  String dateConversion(String convertDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
		String convertedDate = null;
		try {
			Date date = sdf.parse(convertDate);
			convertedDate = sdf.format(date);
		} catch (ParseException e) 
		{
			Log.e("DATE CONVERSION", convertedDate);
		}

		return convertedDate;

	}
	
}

package com.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper

{

	private static final String DATABASE_NAME = "appointmentfunctionality.db";
	private static final String REMINDER_TABLE_NAME = "reminder";
	private static final int DATABASE_VERSION = 3;

	private static final String REMIDNER_ID = "reminder_id";
	private static final String REMINDER_EVENET_ID = "reminder_event_id";
	private static final String REMINDER_EVENT_NAME = "reminder_event_title";
	private static final String REMINER_EVENT_DESCRIPTION = "reminder_event_description";
	private static final String EVENT_ADDRESS = "event_address";
	private static final String EVENT_START_DATE = "event_statrt_date";
	private static final String EVENT_END_DATE = "event_end_date";
	private static final String EVENT_START_TIME = "event_start_time";
	private static final String EVENT_END_TIME = "event_end_time";
	private static final String USER_REMINDER_DATE = "user_reminder_date";
	private static final String USER_REMIDNER_TIME = "user_reminder_time";
	private static final String USER_ID = "user_id";
	private static final String NOTIFY_EMAIL_STATUS = "notify_reminder_status";

	private String CREATE_TABLE = "CREATE TABLE " + REMINDER_TABLE_NAME + "("
			+ REMIDNER_ID + " INTEGER PRIMARY KEY," + REMINDER_EVENET_ID
			+ " TEXT," + REMINDER_EVENT_NAME + " TEXT,"
			+ REMINER_EVENT_DESCRIPTION + " TEXT," + EVENT_ADDRESS + " TEXT,"
			+ EVENT_START_DATE + " DATE," + EVENT_END_DATE + " DATE,"
			+ EVENT_START_TIME + " TEXT," + EVENT_END_TIME + " TEXT,"
			+ USER_REMINDER_DATE + " DATE," + USER_REMIDNER_TIME + " TEXT,"
			+ USER_ID + " TEXT," + NOTIFY_EMAIL_STATUS + " TEXT" + ")";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
		Log.e("DATABASE", "CREATED");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String reminderTableSqlString = "DROP TABLE IF EXISTS " + REMINDER_TABLE_NAME;
		db.execSQL(reminderTableSqlString);
		Log.e("DATABASE", "UPGRADE");
		onCreate(db);
	}

}

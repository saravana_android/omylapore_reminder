package com.Services;

import java.io.InputStream;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.classitem.EventsItem;
import com.utills.GlobalVariables;

public class EventsService {

	private String serviceURL = "http://omylapore.com/classes/events-news.php";
	ArrayList<EventsItem> eventsList;
	Context context;
    InputStream is = null;
    JSONObject jObj = null;
    String result = "";
   
	public EventsService(Context context) { 
		this.context = context;
	}

	public void getEvents(String page) {
	
		eventsList = new ArrayList<EventsItem>();
		
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("todo","get_all_events"));
        params.add(new BasicNameValuePair("cat_code", "ALL"));
        params.add(new BasicNameValuePair("page", page));
        params.add(new BasicNameValuePair("rslt_per_page", "10"));
        
        jObj = JSONParser.getJSONFromUrl(serviceURL, params);
        
         try {
			String totalCount = jObj.getString("totalCount");
			EventsItem.setTotalcount(jObj.getString("tot_events"));
	
			for (int i = 0; i < Integer.parseInt(totalCount); i++) {
				JSONObject eventsJsonObj = jObj.getJSONObject(String.valueOf(i));
				
				EventsItem eventObj = new EventsItem();
				eventObj.setEventTitle(eventsJsonObj.getString("event_id"));
				Log.e("SERVICE_EVENT_ID", " "+eventsJsonObj.getString("event_id"));
				eventObj.setEventTitle(eventsJsonObj.getString("event_title"));
				
				eventObj.setEventFromDate(eventsJsonObj.getString("event_from_date"));
				eventObj.setEventToDate(eventsJsonObj.getString("event_to_date"));
				eventObj.setEventAddress(eventsJsonObj.getString("event_address"));
				eventObj.setEventContacPhone(eventsJsonObj.getString("event_contact_phone"));
				eventObj.setEventDescription(eventsJsonObj.getString("event_description"));
				eventsList.add(eventObj);

			}
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
         						
			GlobalVariables.totalEventsList.addAll(eventsList);
	}

}

package com.classitem;

public class EventsItem {

	private String eventId;
	private String eventTitle;
	private String eventCategoryCode;
	private String eventFromDate;
	private String eventToDate;
	private String eventDescription;
	private String eventAddress;
	private String eventBanner;
	private String eventContacPerson;
	private String eventContacPhone;
	private String eventContactMail;
	private static String totalcount;

	private String eventStartTime;
	private String eventEndTime;
	private String userReminderTime;
	private String userReminderDate;
	private String userId;
	private String reminderNotification;

	public EventsItem(){
		
	}
	public EventsItem(String eventId, String eventTitle,
			String eventDescription, String eventFromDate, String eventToDate,
			String eventAddress, String eventStartTime, String eventEndTime,
			String userReminderTime, String userReminderDate, String userId,
			String reminderNotification) {
		
		this.eventId = eventId;
		this.eventTitle = eventTitle;
		this.eventDescription = eventDescription;
		this.eventFromDate = eventFromDate;
		this.eventToDate = eventToDate;
		this.eventAddress = eventAddress;
		this.eventStartTime = eventStartTime;
		this.eventEndTime = eventEndTime;
		this.userReminderTime = userReminderTime;
		this.userReminderDate = userReminderDate;
		this.userId = userId;
		this.reminderNotification = reminderNotification;
	}

	public static String getTotalcount() {
		return totalcount;
	}

	public static void setTotalcount(String totalcount) {
		EventsItem.totalcount = totalcount;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getEventTitle() {
		return eventTitle;
	}

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	public String getEventCategoryCode() {
		return eventCategoryCode;
	}

	public void setEventCategoryCode(String eventCategoryCode) {
		this.eventCategoryCode = eventCategoryCode;
	}

	public String getEventFromDate() {
		return eventFromDate;
	}

	public void setEventFromDate(String eventFromDate) {
		this.eventFromDate = eventFromDate;
	}

	public String getEventToDate() {
		return eventToDate;
	}

	public void setEventToDate(String eventToDate) {
		this.eventToDate = eventToDate;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public String getEventAddress() {
		return eventAddress;
	}

	public void setEventAddress(String eventAddress) {
		this.eventAddress = eventAddress;
	}

	public String getEventBanner() {
		return eventBanner;
	}

	public void setEventBanner(String eventBanner) {
		this.eventBanner = eventBanner;
	}

	public String getEventContacPerson() {
		return eventContacPerson;
	}

	public void setEventContacPerson(String eventContacPerson) {
		this.eventContacPerson = eventContacPerson;
	}

	public String getEventContacPhone() {
		return eventContacPhone;
	}

	public void setEventContacPhone(String eventContacPhone) {
		this.eventContacPhone = eventContacPhone;
	}

	public String getEventContactMail() {
		return eventContactMail;
	}

	public void setEventContactMail(String eventContactMail) {
		this.eventContactMail = eventContactMail;
	}

	public String getEventStartTime() {
		return eventStartTime;
	}

	public void setEventStartTime(String eventStartTime) {
		this.eventStartTime = eventStartTime;
	}

	public String getEventEndTime() {
		return eventEndTime;
	}

	public void setEventEndTime(String eventEndTime) {
		this.eventEndTime = eventEndTime;
	}

	public String getUserReminderTime() {
		return userReminderTime;
	}

	public void setUserReminderTime(String userReminderTime) {
		this.userReminderTime = userReminderTime;
	}

	public String getUserReminderDate() {
		return userReminderDate;
	}

	public void setUserReminderDate(String userReminderDate) {
		this.userReminderDate = userReminderDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getReminderNotification() {
		return reminderNotification;
	}

	public void setReminderNotification(String reminderNotification) {
		this.reminderNotification = reminderNotification;
	}

}

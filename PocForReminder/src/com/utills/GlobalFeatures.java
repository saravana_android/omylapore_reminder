package com.utills;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.pocforreminder.MainActivity;
import com.example.pocforreminder.R;
import android.annotation.SuppressLint;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;

public class GlobalFeatures {

	@SuppressLint("InlinedApi")
	public static void callMainActivity(Context context) {
		Intent mainIntent = new Intent(context, MainActivity.class);
		mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		context.startActivity(mainIntent);
	}

	public static void callLocationAccess(Context context) {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		context.startActivity(intent);
	}

	public static boolean haveNetworkConnection(Context context) {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	public static void showAlertDialog(Context context) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle("Connection Error");
		alertDialog
				.setMessage("Please check your internet connection and try again");
		alertDialog.setIcon(R.drawable.abc_ab_bottom_solid_light_holo);
		alertDialog.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

					}
				});
		alertDialog.show();
	}

	public static void makeCall(Context context, String contact_no) {

		Intent call_intent = new Intent(Intent.ACTION_DIAL);
		call_intent.setData(Uri.parse("tel:" + contact_no));
		context.startActivity(call_intent);
	}

	public static void sendSms(Context context, String addr) {

		Intent smsIntent = new Intent(Intent.ACTION_VIEW);
		smsIntent.setData(Uri.parse("smsto:"));
		smsIntent.setType("vnd.android-dir/mms-sms");
		smsIntent.putExtra("sms_body", addr);
		context.startActivity(smsIntent);
	}

	
}
